import io from 'socket.io-client'
import store from '../store'

const socket = io()

socket.on('connect', () => {
  console.log('socket server connected succ.');
})

// 获取房间信息
socket.on('roomInfo', ({ nicknames, holder}) => {
  store.commit('updateUserList', nicknames)
  store.commit('updateHolder', holder)
})

// 其他用户进入房间
socket.on('userEnter', nickname => {
  store.commit('addUserToList', nickname)
})

// 用户离开
socket.on('userLeave', nickname => {
  store.commit('delUserFromList', nickname)
})

// 开始画线
socket.on('newLine', line => {
  store.commit('drawNewLine', line)
})

// 同步开始状态给其他玩家
socket.on('newLineCallback', line => {
  store.commit('drawNewLine', line)
})

socket.on('updateLineCallback', lastLine => {
  store.commit('updateLine', lastLine)
})

// 同步更新状态给其他玩家

export default socket