import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import VueKonva from 'vue-konva'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.config.productionTip = false

Vue.use(ElementUI)
Vue.use(VueKonva)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
