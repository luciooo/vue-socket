module.exports = {
  publicPath: "./",
  devServer: {
    proxy: {
      '/': {
        target: 'http://106.14.165.51:3000/',
        changeOrigin: true
      }
    }
  }
}