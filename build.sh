#! /bin/bash

echo 'start build...'

npm run build

tar -cvf dist.tar dist/

scp -r dist.tar root@106.14.165.51:/root/static/vue-socket

ssh root@106.14.165.51 'sh /root/static/sbin/vue-socket.sh'